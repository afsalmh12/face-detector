


let flipButton = false, deviceId = undefined, devices = [], videoElement, canvas,
  selectedImage = null,
  frontCamera = true,
  model = null,
  ctx = null,
  detector,
  submitBtn,
  nameInput,
  personName,
  face;

const apiUrls = {
  faceDetector: 'http://localhost:3002/api/face-detector',
  registerNewFace: 'http://localhost:3002/api/register-face'
};
const defaultImage = new Image();
defaultImage.src = 'face.jpg';


const fetchPOST = async (url, body) => {
  const response = await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  });
  return response.json();
}

const getCameraSelection = async () => {
  let permissionEnable = true
  const devices = await navigator.mediaDevices.enumerateDevices();
  const videoDevices = devices.filter(device => device.kind === 'videoinput');
  deviceId = videoDevices && videoDevices.length ? videoDevices[0].deviceId : undefined
  videoDevices.forEach((device) => {
    devices.push(device.deviceId)
    console.log(device)
    if (!device.label) {
      permissionEnable = false
    }
  })
  flipButton = videoDevices.length > 1 ? true : false
  startVideoElement()
  if (!permissionEnable) {
    console.log("permission denied")
  }
};


const snap = () => {
  ctx.imageSmoothingEnabled = false;
  ctx.drawImage(videoElement, 0, 0, canvas.width, canvas.height);
  stopVideoElement();
}

const showImage = (show) => {
  if (show) {
    videoElement.classList.add('hide-me');
    canvas.classList.remove('hide-me');
  } else {
    videoElement.classList.remove('hide-me');
   // canvas.classList.add('hide-me');
  }
}

const showName = (input, display) => {
  if (!input && !display) {
    nameInput.classList.add('hide-me');
    submitBtn.classList.add('hide-me');
    personName.classList.add('hide-me');
  }
  else if (input && !display) {
    nameInput.classList.remove('hide-me');
    submitBtn.classList.remove('hide-me');
    personName.classList.add('hide-me');
  }
  else if (!input && display) {
    nameInput.classList.add('hide-me');
    submitBtn.classList.add('hide-me');
    personName.classList.remove('hide-me');
  }
}

const stopVideoElement = () => {
  try {
    showImage(true);
    const stream = videoElement.srcObject;
    if (!stream) return;
    const tracks = stream.getTracks();
    tracks.forEach(function (track) {
      track.stop();
    });
    videoElement.srcObject = null;
  } catch (e) {
    console.error(e)
  }

}
const startVideoElement = () => {
  showImage(false);
  showName(false, false);
  if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
    navigator.mediaDevices.getUserMedia({ video: { deviceId: deviceId } }).then((stream) => {
      videoElement.srcObject = stream
      console.log("acess granted")
    }).catch(handleError).finally(() => {
      console.log("Capturing in ", deviceId)
      setTimeout(() => detectFaces(), 3000)
    })
  } else {
    toaster.error('Sorry, camera not available')
  }
}



const flipCamera = () => {
  stopVideoElement()
  if (devices[0]?.length > 1)
    deviceId = deviceId === devices[0] ? devices[1] : devices[0]
  startVideoElement();
}

const handleError = (error) => {
  console.log('Error: ', error);
}

const registerNewFace = () => {
  const name = nameInput.value.trim();
  if (face && name) {
    fetchPOST(apiUrls.registerNewFace, { face, name }).then((res) => startVideoElement()).catch((err) => {
      console.log(err);
      startVideoElement();
    })
  }
}

const detectFaces = () => {
  const estimationConfig = { flipHorizontal: false };
  detector.estimateFaces(videoElement, estimationConfig).then(async (faces) => {
    snap();
    if (faces?.length) {
      const detectedFace = await fetchPOST(apiUrls.faceDetector, { face: faces[0] });
      face = faces[0];
      if (!detectedFace) showName(true, false);
      else {
        personName.textContent = `Hey, ${detectedFace.name}`
        showName(false, true);
      }
    }
  }).catch((e) => console.log(e));

};

const getElements = () => {
  videoElement = document.getElementById('video');
  canvas = document.getElementById('canvas');
  nameInput = document.getElementById('name');
  submitBtn = document.getElementById('submitBtn');
  personName = document.getElementById('displayName');
}


const clickEvents = {
  //snapBtn: snap,
  flipBtn: flipCamera,
  stopBtn: stopVideoElement,
  submitBtn: registerNewFace
}

document.addEventListener("DOMContentLoaded", async function () {
  getElements();
  Object.keys(clickEvents).forEach((btn) => {
    document.getElementById(btn).addEventListener('click', clickEvents[btn]);
  })
  videoElement.addEventListener('loadedmetadata', () => {
    canvas.width = videoElement.videoWidth;
    canvas.height = videoElement.videoHeight;
  });
  ctx = canvas.getContext('2d');
  nameInput.addEventListener("input", () => { submitBtn.disabled = nameInput.value ? false : true });
  getCameraSelection()
  
  const model = faceDetection.SupportedModels.MediaPipeFaceDetector;
  const detectorConfig = {
    runtime: 'mediapipe',
    solutionPath: 'https://cdn.jsdelivr.net/npm/@mediapipe/face_detection',
    // or 'base/node_modules/@mediapipe/face_detection' in npm.
  };
  detector = await faceDetection.createDetector(model, detectorConfig);
})