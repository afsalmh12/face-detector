
const express = require('express');
const app = express();
const mongoose = require("mongoose");
const cors = require('cors')
const matchingThreshold = 50;

const schema = new mongoose.Schema({ name: String, face: Object });
const Face = mongoose.model('Face', schema);
const createNewFace = async (face) => {
    return await Face.create(face);
}
const faces = async () => {
    return await Face.find({});
}

const configureDB = () => {
    const url = 'mongodb://localhost:27017/facedetector'
    mongoose.connect(url).then((conn) => console.log("DB Connected")).catch((err) => console.log("database connection error ",err));
} 

const registerAPIEndPoints = () => {
    app.get('/',(req,res) => res.send("Welcome to Face detector REST API Service"));
    app.post('/api/face-detector',faceDetector)
    app.post('/api/register-face',registerNewFace)
}
const startServer = () => {
    let port = portNumber()
    app.use(cors());
    app.use(express.urlencoded({ extended: false }));
    app.use(express.json({limit:'10mb'}));
    registerAPIEndPoints();
    app.listen(port,() => console.log("Server started : Port : ",port))
    configureDB();
}
//to get the port number from environment file
const portNumber = () => {
    return Number(process.env.PORT) || 3002
}


  
  // Function to calculate Euclidean distance between two points
  const euclideanDistance = (p1, p2) => {
    const dx = p1.x - p2.x;
    const dy = p1.y - p2.y;
    return Math.sqrt(dx * dx + dy * dy);
  }
  
  // Function to compare a new face with known faces and find the best match
  const recognizeFace = (newFace,knownFaces) => {
    let bestMatch = null;
    let minDistance = Number.MAX_VALUE;
  
    for (const knownFace of knownFaces) {
      // Calculate the distance between newFace and knownFace using keypoints
      const distance = euclideanDistance(newFace.keypoints[0], knownFace.face.keypoints[0]);
  
      // Update bestMatch if the distance is smaller
      console.log(distance)
      if (distance < minDistance && distance <= matchingThreshold) {
        minDistance = distance;
        bestMatch = knownFace;
      }
    }
  
    return bestMatch;
  }
    
    

const faceDetector = async (req,res) => {
    const allFaces = await faces();
    const body = req.body;
    const matchedFace = recognizeFace(body.face,allFaces);
    return res.status(200).json(matchedFace)
}

const registerNewFace = async (req,res) => {
    const face = req.body;
    const result = await createNewFace(face);
    return res.status(200).json(result);
}

startServer();